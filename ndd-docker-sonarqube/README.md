# NDD Docker Sonarqube



## Installation

Clone the repository:

```shell
git clone git@bitbucket.org:ndd-docker/ndd-docker-sonarqube.git
cd ndd-docker-sonarqube
```



## Usage

<!--
NB: the script `bin/caches.sh` provides some helper utilities (`./bin/caches.sh --help`).

### Initialisation

Build the images:

```shell
docker-compose build
```

A volume should be created in order to persist data:

```shell
docker volume create --name ddidier-sonarqube-data
```

### From the host

Then start all containers with:

```shell
docker compose up
```

Once running:

- the HTTPd proxy is available at `http://localhost:12080`
- The APT Cacher NG console is available `http://localhost:12081/acng-report.html`

### From a building image

To use these caches from a building a Docker image, you need to connect the proxy to the default `bridge` network:

```shell
docker network connect bridge ddidier-http-cache
```

Then find the IP of the HTTPd proxy in this network:

```shell
export DOCKER_HTTP_PROXY="http://$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ddidier-http-cache)"
```

Then pass it as a build argument, for example:

```shell
docker build \
    --build-arg http_proxy=$DOCKER_HTTP_PROXY \
    --build-arg HTTP_PROXY=$DOCKER_HTTP_PROXY \
    -t my/image .
```
 -->
