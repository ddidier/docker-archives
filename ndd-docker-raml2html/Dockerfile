#
# ddidier/raml2html
#
# A Docker image for the RAML to HTML documentation generator (https://github.com/raml2html/raml2html).
#
# docker build -t ddidier/raml2html .

FROM       node:6.10.2
MAINTAINER David DIDIER

ENV RAML2HTML_VERSION 6.3.0

RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install --no-install-recommends -y curl \
 && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
 && curl -o /usr/local/bin/gosu     -SL "https://github.com/tianon/gosu/releases/download/1.10/gosu-$(dpkg --print-architecture)" \
 && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.10/gosu-$(dpkg --print-architecture).asc" \
 && gpg --verify /usr/local/bin/gosu.asc \
 && rm /usr/local/bin/gosu.asc \
 && chmod +x /usr/local/bin/gosu \
 && apt-get autoremove -y \
 && rm -rf /var/cache/* \
 && rm -rf /var/lib/apt/lists/* \
 && npm install --global "raml2html@${RAML2HTML_VERSION}"

COPY files/usr/local/bin/docker-entrypoint /usr/local/bin/

RUN chown root:root /usr/local/bin/docker-entrypoint \
 && chmod 755 /usr/local/bin/docker-entrypoint

ENV DATA_DIR /data

WORKDIR $DATA_DIR

# RUN npm install --global "raml2html-markdown-theme@1.0.1"

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
