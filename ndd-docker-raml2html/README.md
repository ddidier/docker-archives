# NDD Docker RAML2HTML

A Docker image for the [RAML to HTML documentation generator](https://github.com/raml2html/raml2html).

The image is based upon the official [NodeJS](https://hub.docker.com/_/node/).



## Installation

### From source

```
git clone git@bitbucket.org:ndd-docker/ndd-docker-raml2html.git
cd ndd-docker-raml2html
docker build -t ddidier/raml2html .
```

### From Docker Hub

```
docker pull ddidier/raml2html
```



## Usage

TODO
